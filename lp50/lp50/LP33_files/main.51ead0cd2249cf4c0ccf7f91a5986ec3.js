$(function () {
  $(".next").click(function (e) {
    e.preventDefault();
    $(this).parent().hide().next().fadeIn();
  });

  $(".gonext").click(function (e) {
    e.preventDefault();
    $(".step1").hide().next().fadeIn();
  });

  /* Change girl picture */
  var curId = 1;
  $(document).on("click", ".changeGirl", function (e) {
    e.preventDefault();
    currentId = "#images" + curId;

    curId++;
    nextId = "#images" + curId;

    if (curId > 3) {
      return;
    }

    console.log("Changing to " + nextId + " frrom " + currentId);
    $(currentId).fadeOut("fast");
    $(nextId).fadeIn();
  });

  $(document).on("click", ".run_loading", function (e) {
    $(".content").hide();
    $("#questions-content").hide();
    $("#agree-content").fadeIn();
    $("#agree").focus();
  });

  $(document).on("click", ".selectable-image", function (e) {
    $(this).addClass("seletectedImage");
  });
});
