var redirectionURL = "#";
var redirectionParam = "";
jQuery.extend({
  getURLParam: function (strParamName) {
    var strReturn = "";
    var strHref = window.location.href;
    var bFound = false;
    var cmpstring = strParamName + "=";
    var cmplen = cmpstring.length;
    if (strHref.indexOf("?") > -1) {
      var strQueryString = strHref.substr(strHref.indexOf("?") + 1);
      var aQueryString = strQueryString.split("&");
      for (var iParam = 0; iParam < aQueryString.length; iParam++) {
        if (aQueryString[iParam].substr(0, cmplen) == cmpstring) {
          var aParam = aQueryString[iParam].split("=");
          strReturn = aParam[1];
          bFound = true;
          break;
        }
      }
    }
    if (bFound == false) return null;
    return strReturn;
  },
});
jQuery.extend({
  getURLParamString: function () {
    var strReturn = "";
    var strHref = window.location.href;
    if (strHref.indexOf("?") > -1) {
      strReturn = strHref.substr(strHref.indexOf("?") + 1);
    }
    return strReturn;
  },
});
jQuery.extend({
  removeItemFromArray: function (arr, item) {
    var tmp = [];
    for (var index in arr) {
      var oneparam = arr[index];
      if (oneparam.indexOf(item) == 0) {
        continue;
      }
      tmp.push(oneparam);
    }
    return tmp;
  },
});
jQuery.extend({
  getURLParamStringWithoutBackground: function () {
    var strReturn = "";
    var strHref = window.location.href;
    if (strHref.indexOf("?") > -1) {
      var strParams = strHref.substr(strHref.indexOf("?") + 1);
      var splitted = strParams.split("&");
      splitted = $.removeItemFromArray(splitted, "bgrnd");
      splitted = $.removeItemFromArray(splitted, "bgcolor");
    }
    for (var index in splitted) {
      if (strReturn != "") {
        strReturn = strReturn + "&";
      }
      strReturn = strReturn + splitted[index];
    }
    return strReturn;
  },
});
(function () {
  var stringParam = $.getURLParamStringWithoutBackground();
  var fullRedirUrl = redirectionURL + "?" + stringParam + redirectionParam;
  $("#next").attr("href", fullRedirUrl);
  var lang = $.getURLParam("lang");
  if (lang != null) {
    var translator = $("body").translate({ lang: lang, t: dict });
    var translation = translator.get("HEADLINE");
    document.title = translation;
  }
  var background = $.getURLParam("bgrnd");
  if (background != null) {
    $("body").css(
      "background-image",
      "url(" + "/background/" + background + ")"
    );
  }
  var fullbg = $.getURLParam("f");
  if (fullbg != null) {
    $("body").css("background-repeat", "no-repeat");
    $("body").css("background-size", "100%");
  }
  var bgcolor = $.getURLParam("bgcolor");
  if (bgcolor != null) {
    $(".panel-heading-colorchange").css("background-color", bgcolor);
  }
  function run_loading_run_1(a) {
    $("#ok").removeClass("invisible").show();
    timeoutID1 = window.setTimeout(run_loading_1, a);
  }
  function run_loading_1() {
    $("#s0").fadeIn().removeClass("invisible");
  }
  function run_loading_run_2(a) {
    timeoutID1 = window.setTimeout(run_loading_2, a);
  }
  function run_loading_2() {
    $("#s1").fadeIn().removeClass("invisible");
  }
  function run_loading_run_3(a) {
    timeoutID1 = window.setTimeout(run_loading_3, a);
  }
  function run_loading_3() {
    $("#s2").fadeIn().removeClass("invisible");
  }
  function run_loading_run_4(a) {
    timeoutID1 = window.setTimeout(run_loading_4, a);
  }
  function run_loading_4() {
    $("#s3").fadeIn().removeClass("invisible");
  }
  function run_loading_run_5(a) {
    timeoutID1 = window.setTimeout(run_loading_5, a);
  }
  function run_loading_5() {
    $("#s4").fadeIn().removeClass("invisible");
    $(".loading").hide();
    $("#weiter").removeClass("invisible").show();
    $("#next").removeClass("invisible").show();
  }
  function nextStep() {
    var $step = $(".current").attr("rel");
    $(".current")
      .removeClass("current")
      .fadeOut("slow", "swing", function () {
        $(".next")
          .fadeIn("slow")
          .removeClass("invisible")
          .addClass("current")
          .removeClass("next");
        var next = parseInt($step) + 2;
        var nextid = "#q" + next;
        if ($(nextid).length) {
          $(nextid).removeClass("current").addClass("next");
        }
        if ($(this).hasClass("last")) {
          $("#yesno").hide();
          $("#rasiert").hide();
          run_loading_run_1("1500");
          run_loading_run_2("3550");
          run_loading_run_3("5500");
          run_loading_run_4("6500");
          run_loading_run_5("7500");
        }
      });
    if ($step == "4") {
      $("#yesno").hide();
      $("#rasiert").fadeIn("slow").removeClass("invisible");
    }
  }
  $("#yesno button").on("click", nextStep);
  $("#rasiert button").on("click", nextStep);
  var countdown = "300";
  function doCount() {
    if (countdown > 0) {
      countdown--;
    } else {
    }
    var s = countdown;
    var h = Math.floor(s / 3600);
    var m = Math.floor((s - h * 3600) / 60);
    s = (s - h * 3600) % 60;
    $("#counter").html(
      addLeadingZeros(h, 2) +
        ":" +
        addLeadingZeros(m, 2) +
        ":" +
        addLeadingZeros(s, 2)
    );
    window.setTimeout(function () {
      doCount();
    }, 1000);
  }
  doCount();
  function addLeadingZeros(n, length) {
    var str = (n > 0 ? n : -n) + "";
    var zeros = "";
    for (var i = length - str.length; i > 0; i--) zeros += "0";
    zeros += str;
    return n >= 0 ? zeros : "-" + zeros;
  }
})(jQuery);
